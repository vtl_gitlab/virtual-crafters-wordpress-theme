![vtl-images](https://i.imgur.com/wN3ytqj.png)

# Virtual Crafters Wordpress theme

A starter wordpress theme with strong **gulp** configurations. Following the semi 7-1 architecture by _Hugo giraudel_ and implementing best practices.

Here is the [official Documentation](https://vtl_gitlab.gitlab.io/virtual-crafters-wordpress-theme/) of the theme.

> The theme is inspired by the theme _twentytwenty_ and _underscores_ theme.

### Quickstart

#### Clone the repo

```
git clone https://gitlab.com/vtl_gitlab/virtual-crafters-wordpress-theme.git
```

### Install npm dependencies

```
# Install dependencies
npm install # OR yarn install
```

### **Start hacking 🎉**
