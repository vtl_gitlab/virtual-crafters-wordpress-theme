<?php
/* 
Template Name: About Us
*/

get_header();
?>
<main class="about">
  <section class="about-us py-5 " id="about-us">
    <div class="container mt-5">
      <div class="row">
        <div class="col-md-4">
          <h1 class='text-success'>Welcome! alskjdf awoeijf</h1>
          <h2>Know More About Us</h2>
          <hr>
          <p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore etae magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
          <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          <button type="button" class="btn btn-success">Let's Know More</button>

        </div>
        <div class="col-md-8">
          <img data-src="http://themebubble.com/demo/marketingpro/wp-content/uploads/2016/10/seo-slide.png " alt="">
        </div>
        <?php get_template_part('template-parts/newsletter'); ?>
        <?php get_template_part('template-parts/testimonials'); ?>
      </div>
      <div class="row">
        <div class="col-12">
          <?php
          if (have_posts()) :
            wp_reset_query();
            while (have_posts()) :
              the_post();
              the_content();
            endwhile;
          endif;
          ?>
        </div>
      </div>
    </div>
  </section>

</main>

<?php
get_footer();
?>