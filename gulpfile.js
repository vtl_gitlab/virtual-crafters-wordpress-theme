// Gulp
const { src, dest, watch, series, parallel } = require("gulp");

// Css
const prefix = require("autoprefixer");
const minify = require("cssnano");
const sourcemaps = require("gulp-sourcemaps");
const sass = require("gulp-sass");
const postcss = require("gulp-postcss");
const wait = require("gulp-wait");

// Javascript
const babel = require("gulp-babel");
const uglify = require("gulp-uglify");

// Other
const rename = require("gulp-rename");
const gutil = require("gulp-util");
const ftp = require("vinyl-ftp");
const bs = require("browser-sync").create();
const sftp = require("gulp-sftp-up4");
// const notify = require("gulp-notify");
const plumber = require("gulp-plumber");

// FTP Configuration
const ftpConfig = {
  host: "ftp.vtlcrafters.com",
  user: "developer@vc-preview.com",
  pass: "M@ur!c3M0ss2006",
  remoteFolder: "/renz/wp-content/themes/vtl",
  siteUrl: "http://vc-preview.com/renz/"
};

const files = {
  scssPath: "./css/scss/**/*.scss",
  jsPath: "./js/scripts/*.js",
  assetsPath: "./assets/images/*"
};

var globs = [
  "./assets/fonts/**/*",
  "./assets/images/*",
  "./js/**",
  "./css/**",
  "./classes/*",
  "./inc/**",
  "./page-templates/*",
  "./template-parts/**",
  "./*",
  "!gulpfile.js",
  "!node_modules/**"
];

var settings = {
  scripts: true,
  styles: true,
  reload: true
};

var scssTask = function(done) {
  if (!settings.styles) return done();
  return src(files.scssPath)
    .pipe(wait(100))
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass())
    .on("error", sass.logError)
    .pipe(
      postcss([
        prefix({
          cascade: true,
          remove: true
        }),
        minify()
      ])
    )
    .pipe(sourcemaps.write())
    .pipe(rename("main.css"))
    .pipe(dest("./css/"));
};

var jsTask = function(done) {
  if (!settings.scripts) return done();
  return src(files.jsPath)
    .pipe(plumber())
    .pipe(
      babel({
        presets: ["@babel/preset-env"]
      })
    )
    .pipe(uglify())
    .pipe(rename("script.js"))
    .pipe(dest("./js/"));
};

function getConn() {
  return ftp.create({
    host: ftpConfig.host,
    user: ftpConfig.user,
    password: ftpConfig.pass,
    parallel: 5,
    log: gutil.log
  });
}

function deploy() {
  var conn = getConn();

  return src(globs, { base: ".", buffer: false })
    .pipe(
      plumber(() => {
        gutil.log(error.message + " --Run gulp to retry...");
        this.emit("end");
      })
    )
    .pipe(conn.newer(ftpConfig.remoteFolder))
    .pipe(conn.dest(ftpConfig.remoteFolder));
}

var startServer = function(done, site) {
  if (!settings.reload) return done();
  bs.init({
    proxy: {
      target: ftpConfig.siteUrl
    },
    port: 3000,
    watchOptions: {
      ignored: "node_modules/*",
      ignoreInitial: true
    }
  });
  done();
};

var startServerSftp = function(done) {
  if (!settings.reload) return done();
  bs.init({
    proxy: {
      target: sftpConfig.site
    },
    port: 3000,
    watchOptions: {
      ignored: "node_modules/*",
      ignoreInitial: true
    }
  });
  done();
};

var conn = getConn();

var sftpConfig = {
  site: "https://dev-gulp-sftp.pantheonsite.io/",
  conn: {
    host: "appserver.dev.08124f94-6e79-45d9-8766-092d76c3a943.drush.in",
    user: "dev.08124f94-6e79-45d9-8766-092d76c3a943",
    pass: "admin",
    // passphrase: "admin",
    port: "2222",
    remotePath:
      "/srv/bindings/0d43a355a39e4f5ca0bd44ba35abc8bb/code/wp-content/themes/vtl/"
  }
};

function deploySftp() {
  return src(["./*", "./**", "!gulpfile.js", "!node_modules/**"]).pipe(
    sftp(sftpConfig)
  );
}

var watchFiles = function(done) {
  // Watch Js files
  watch([files.jsPath]).on("change", () => {
    jsTask();
  });

  // Watch Scss files
  watch([files.scssPath]).on("change", () => {
    scssTask();
  });

  // Get file changes and upload to ftp
  watch(["./**", "./*"]).on("change", event => {
    return src([event], { base: ".", buffer: true })
      .pipe(plumber())
      .pipe(conn.newer(ftpConfig.remoteFolder))
      .pipe(conn.dest(ftpConfig.remoteFolder))
      .pipe(bs.stream(true));
    // .pipe(notify("File Changed: <%= file.relative %>"))
  });
  done();
};

var watchFilesSftp = function(done) {
  // Watch Js files
  watch([files.jsPath]).on("change", () => {
    jsTask();
  });

  // Watch Scss files
  watch([files.scssPath]).on("change", () => {
    scssTask();
  });

  // Get file changes and upload to ftp
  watch(["./**", "./*"]).on("change", event => {
    return src([event]).pipe(sftp(sftpConfig.conn));
  });
  done();
};

exports.watchSftp = series(watchFilesSftp);
exports.deploySftp = deploySftp;
exports.deploy = deploy;
exports.default = series(startServer, watchFiles);
