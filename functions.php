<?php
include('inc/custom-shortcodes.php');
// require get_template_directory_uri() . '/classes/theme-class.php';
include('classes/theme-class.php');
$theme = new MyTheme;
$jsVendors = '/js/scripts/vendors';
$cssVendors = '/css/scss/vendors';
$theme->addStyle('fontawesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css')
  ->addStyle('bootstrap-css', get_template_directory_uri() . $cssVendors . '/bootstrap/bootstrap.min.css')
  ->addScript('bootstrap-js-bundle', get_template_directory_uri() . $jsVendors . '/bootstrap/bootstrap.bundle.min.js', array('jquery'))
  ->addScript('bootstrap-js', get_template_directory_uri() . $jsVendors . '/bootstrap/bootstrap.min.js')
  ->addStyle('slick-css', get_template_directory_uri() . $cssVendors . '/slick/slick.css')
  ->addStyle('slick-theme', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css')
  ->addScript('slick-js', get_template_directory_uri() . $jsVendors . '/slick/slick.js')
  ->addScript('main-scripts', get_template_directory_uri() . '/js/script.js', array('jquery'))
  ->addStyle('main-styles', get_template_directory_uri() . '/css/main.css');

function register_menus()
{
  $locations = array(
    'top-primary' => __('Custom Primary', 'Virtual Crafters Theme')
  );

  register_nav_menus($locations);
}

add_action('init', 'register_menus');

// Register nav walker
function register_navwalker()
{
  require_once get_template_directory() . '/inc/bootstrap-navwalker.php';
}
add_action('after_setup_theme', 'register_navwalker');


// -- Widgets
function wpb_widgets_init()
{

  register_sidebar(array(
    'name' => __('Menu Widget', 'wpb'),
    'id' => 'sidebar-1',
    'description' => __('The main sidebar appears on the right on each page except the front page template', 'wpb'),
    'before_widget' => '<div class="col-xs-12 col-sm-4 col-md-3" id="%1$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ));

  register_sidebar(array(
    'name' => __('Front page sidebar', 'wpb'),
    'id' => 'sidebar-2',
    'description' => __('Appears on the static front page template', 'wpb'),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget' => '</aside>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
  ));
}
add_action('widgets_init', 'wpb_widgets_init');

// -- ACF
if (function_exists('acf_add_options_page')) {
  acf_add_options_page(array(
    'page_title'    => 'Theme General Settings',
    'menu_title'    => 'Theme Settings',
    'menu_slug'     => 'theme-general-settings',
    'capability'    => 'edit_posts',
    'redirect'      => false
  ));

  acf_add_options_sub_page(array(
    'page_title'    => 'Theme CTA Settings',
    'menu_title'    => 'CTA Settings',
    'parent_slug'   => 'theme-general-settings',
  ));
}

// -- Defer Javascripts
function defer_parsing_of_js($url)
{
  if (is_user_logged_in()) return $url; //don't break WP Admin
  if (FALSE === strpos($url, '.js')) return $url;
  if (strpos($url, 'jquery.js')) return $url;
  return str_replace(' src', ' defer src', $url);
}
add_filter('script_loader_tag', 'defer_parsing_of_js', 10);
