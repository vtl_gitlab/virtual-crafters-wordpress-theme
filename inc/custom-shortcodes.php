<?php
// shortcode: list authors
function shapeSpace_list_authors()
{

  $authors = wp_list_authors('optioncount=1&show_fullname=1&exclude_admin=0&hide_empty=1&echo=0');

  return $authors;
}
add_shortcode('list_authors', 'shapeSpace_list_authors');

function the_breadcrumb()
{
  echo '
<ul id="crumbs">';
  if (!is_home()) {
    echo '
    <li><a href="';
    echo get_option('home');
    echo '">';
    bloginfo('name');
    echo "</a></li>
";
    if (is_category() || is_single()) {
      echo '
    <li>';
      the_category('title_li=');
      if (is_single()) {
        echo "</li>
    <li>";
        the_title();
        echo '</li>
';
      }
    } elseif (is_page()) {
      echo '
    <li>';
      echo the_title();
      echo '</li>
';
    }
  } elseif (is_tag()) {
    single_tag_title();
  } elseif (is_day()) {
    echo "
    <li>Archive for ";
    the_time('F jS, Y');
    echo '</li>
';
  } elseif (is_month()) {
    echo "
    <li>Archive for ";
    the_time('F, Y');
    echo '</li>
';
  } elseif (is_year()) {
    echo "
    <li>Archive for ";
    the_time('Y');
    echo '</li>
';
  } elseif (is_author()) {
    echo "
    <li>Author Archive";
    echo '</li>
';
  } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {
    echo "
    <li>Blog Archives";
    echo '</li>
';
  } elseif (is_search()) {
    echo "
    <li>Search Results";
    echo '</li>
';
  }
  echo '</ul>
';
}
add_shortcode('add_breadcrumb', 'the_breadcrumb');

function recent_posts_function($atts)
{
  extract(shortcode_atts(array(
    'posts' => 1,
  ), $atts));

  $return_string = '<ul>';
  query_posts(array('orderby' => 'date', 'order' => 'DESC', 'showposts' => $posts));
  if (have_posts()) :
    while (have_posts()) : the_post();
      $return_string .= '<li><a href="' . get_permalink() . '">' . get_the_title() . '</a></li>';
    endwhile;
  endif;
  $return_string .= '</ul>';

  wp_reset_query();
  return $return_string;
}

function getComName()
{
  if (the_field('company_name', 'option')) :
    the_field('company_name', 'option');
  else :
    return 'Please provide company name info';
  endif;
}
add_shortcode('getCom', 'getComName');

function socials_shortcode($atts)
{
  $atts = shortcode_atts(array(
    'facebook' => '',
    'twitter' => '',
    'instagram' => '',
    'pinterest' => ''
  ), $atts);
  $html = '';
  $html .= '<ul class="list-unstyled list-inline social text-center">';
  foreach ($atts as $key => $att) {
    // $html .= '<li>'  '</li>';
    $html .= (!empty($att)
      ? '<li class="list-inline-item"><a href="' . $att . '"><i class="fa fa-' . $key . '"></i></a></li>'
      : '');
  }
  $html .= '</ul>';
  return $html;
}
add_shortcode('socials', 'socials_shortcode');

function getEmail_shortcode()
{
  return the_field('client_email', 'option');
}
add_shortcode('getEmail', 'getEmail_shortcode');

function socialLinks_shortcode()
{
  return get_template_part(get_template_directory_uri() . '/template-parts/social-links');
}

add_shortcode('socials-acf', 'socialLinks_shortcode');
