<?php
/*  
Template Name: Search
*/
get_header();
?>
<main class="search-page">
  <!-- <section class="section section-search">
    <div class="container custom-container">
      <div class="row">
        <div class="col-12">
          <h1>Search for...</h1>
          <div class="form-group">
            <form class="form-group" id="searchform" method="get" action="<?php echo home_url('/'); ?>">
              <input type="text" class="search-field form-control" name="s" placeholder="Search" value="<?php the_search_query(); ?>">
              <input type="submit" class="form-control d-inline-block" value="Search">
            </form>
          </div>
        </div>
      </div>
    </div>
  </section> -->
  <section class="section section-results">
    <div class="container custom-container">
      <div class="row">
        <div class="col-12">
          <h2>Results Found...</h2>
        </div>
      </div>
      <div class="row results-found">
        <div class="col-12">
          <!-- <div class="results-found__item align-items-center mb-3">
            <?php // get_search_form();
            ?>
          </div> -->
          <?php
          if (have_posts()) :
            while (have_posts()) : the_post();
              // the_content();
          ?>
              <div class="results-found__item align-items-center mb-3">
                <img class="results-found__item__image" src="https://picsum.photos/200/150" alt="">
                <div class="results-found__item__info">
                  <h3><?php the_title(); ?></h3>
                  <span class="d-block"><?php the_permalink(); ?></span>
                  <p><?php the_excerpt(); ?></p>
                </div>
              </div>
          <?php
            endwhile;
          endif;
          ?>
          <!-- <div class="results-found__item align-items-center mb-3">
            <img class="results-found__item__image" src="https://picsum.photos/200/150" alt="">
            <div class="results-found__item__info">
              <h3>About us page</h3>
              <span class="d-block">http://vc-preview.com/renz/about-us</span>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quam, deleniti?</p>
            </div>
          </div>
          <div class="results-found__item align-items-center mb-3">
            <img class="results-found__item__image" src="https://picsum.photos/200/150" alt="">
            <div class="results-found__item__info align-items-center">
              <h3>About us page</h3>
              <span class="d-block">http://vc-preview.com/renz/about-us</span>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quam, deleniti?</p>
            </div>
          </div>
          <div class="results-found__item align-items-center">
            <img class="results-found__item__image" src="https://picsum.photos/200/150" alt="">
            <div class="results-found__item__info align-items-center">
              <h3>About us page</h3>
              <span class="d-block">http://vc-preview.com/renz/about-us</span>
              <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quam, deleniti?</p>
            </div>
          </div> -->
        </div>
      </div>
    </div>
  </section>
</main>
<?php
get_footer();
?>