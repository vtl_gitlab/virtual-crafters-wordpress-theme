<?php
/* 
Template Name: Homepage
*/
get_header();
?>
<main class="homepage">
  <section class="section section01">
    <div class="container">
      <div class="row">
        <div class="col-12 section__banner text-center">
          <img class="d-block mb-5 mx-auto" data-src="<?php echo get_template_directory_uri(); ?>/assets/images/website-logo.png" alt="">
          <a href="https://vtl_gitlab.gitlab.io/virtual-crafters-wordpress-theme/" class="btn btn-primary mr-2">Documentation 18023958109235</a>
          <a href="https://gitlab.com/vtl_gitlab/virtual-crafters-wordpress-theme" class="btn btn-primary">Gitlab Repo fakasdfasdf</a>
        </div>
      </div>
    </div>
  </section>
  <?php get_template_part('template-parts/banner-slider'); ?>
  <?php get_template_part('template-parts/services'); ?>
  <?php get_template_part('template-parts/testimonials'); ?>
  <?php // get_template_part('template-parts/contact-form'); 
  ?>
  <section class="section contact-us-form my-5">
    <div class="container custom-container">
      <div class="row">
        <div class="col-12">
          <?php echo do_shortcode(get_field('form_shortcode'));
          ?>
        </div>
      </div>
    </div>
  </section>
  <?php get_template_part('template-parts/newsletter'); ?>
</main>
<?php
get_footer();
?>