<section class="section contact-us-form my-5">
  <div class="container custom-container">
    <div class="row">
      <div class="col-12">
        <?php echo do_shortcode('[contact-form-7 id="44" title="Contact us"]'); ?>
      </div>
    </div>
  </div>
</section>