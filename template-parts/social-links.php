<ul class="list-unstyled list-inline social text-center">
  <?php
  if (have_rows('social_links_Repeater', 'options')) :
    while (have_rows('social_links_Repeater', 'options')) :
      the_row();
      ?>

      <li class="list-inline-item">
        <a href="<?php echo the_sub_field('social_link_url', 'options'); ?>">
          <?php if (!(the_sub_field('social_link_icon', 'options'))) : ?>
            <i class="<?php echo the_sub_field('social_link_fontawesome', 'options'); ?>"></i>
          <?php else : ?>
            <img data-src="<?php echo the_sub_field('social_link_icon', 'options'); ?>" alt="<?php echo the_sub_field('social_link_icon', 'options'); ?>">
          <?php endif; ?>
        </a>
      </li>

  <?php
    endwhile;
  endif;
  ?>
</ul>