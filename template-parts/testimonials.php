<section class="py-5">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h3 class="text-center">Testimonials</h3>
      </div>
      <div class="col-12">
        <div class="testimonial" id="testimonial-slider">
          <?php
          if (have_rows('testimonials', 'option')) :
            while (have_rows('testimonials', 'option')) : the_row();
          ?>
              <div class="testimonial__item">
                <div class="testimonial__inner">
                  <p><?php the_sub_field('testimonial_paragraph', 'option'); ?></p>
                  <p class="d-inline"><strong><?php the_sub_field('testimonial_name', 'option'); ?></strong></p><br>
                  <span><?php the_sub_field('testimonial_position', 'option'); ?></span>
                </div>
              </div>
            <?php
            endwhile;
          else :
            ?>
            <pre>Insert Testimonials from testimonial options page</pre>
          <?php
          endif;
          ?>
          <!-- <div class="testimonial__item">
            <div class="testimonial__inner">
              <p>"Lorem ipsum dolor, sit amet consectetur adipisicing elit. Maxime necessitatibus enim animi nulla, impedit ipsam? Nobis adipisci quae architecto aliquam!"</p>
              <p class="d-inline"><strong>Havit Mouse</strong></p><br>
              <span>CEO, Stark Industries</span>
            </div>
          </div>
          <div class="testimonial__item">
            <div class="testimonial__inner">
              <p>"Lorem ipsum dolor, sit amet consectetur adipisicing elit. Maxime necessitatibus enim animi nulla, impedit ipsam? Nobis adipisci quae architecto aliquam!"</p>
              <p class="d-inline"><strong>Havit Mouse</strong></p><br>
              <span>CEO, Stark Industries</span>
            </div>
          </div>
          <div class="testimonial__item">
            <div class="testimonial__inner">
              <p>"Lorem ipsum dolor, sit amet consectetur aa dipisicing elit. Maxime necessitatibus enim animi nulla, impedit ipsam? Nobis adipisci quae architecto aliquam!"</p>
              <p class="d-inline"><strong>Havit Mouse</strong></p><br>
              <span>CEO, Stark Industries</span>
            </div>
          </div> -->
        </div>
      </div>
    </div>
  </div>
</section>