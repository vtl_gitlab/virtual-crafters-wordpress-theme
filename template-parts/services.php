<section class="services" id="services">
  <div class="container">
    <div class="row">
      <div class="col-xl-6 mx-auto text-center">
        <div class="section-title mb-100">
          <h4>My Services</h4>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-md-6">
        <!-- Single Service -->
        <div class="single-service">
          <i class="fa fa-laptop"></i>
          <h4>Web Design</h4>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry typesetting industry </p>
        </div>
      </div>
      <div class="col-lg-4 col-md-6">
        <!-- Single Service -->
        <div class="single-service">
          <i class="fa fa-gears"></i>
          <h4>Web Development</h4>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry typesetting industry </p>
        </div>
      </div>
      <div class="col-lg-4 col-md-6">
        <!-- Single Service -->
        <div class="single-service">
          <i class="fa fa-mobile"></i>
          <h4>Responsive Design</h4>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry typesetting industry </p>
        </div>
      </div>
      <div class="col-lg-4 col-md-6">
        <!-- Single Service -->
        <div class="single-service">
          <i class="fa fa-magic"></i>
          <h4>Graphic Design</h4>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry typesetting industry </p>
        </div>
      </div>
      <div class="col-lg-4 col-md-6">
        <!-- Single Service -->
        <div class="single-service">
          <i class="fa fa-pencil"></i>
          <h4>Creative Design</h4>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry typesetting industry </p>
        </div>
      </div>
      <div class="col-lg-4 col-md-6">
        <!-- Single Service -->
        <div class="single-service">
          <i class="fa fa-fa fa-lightbulb-o"></i>
          <h4>Branding</h4>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry typesetting industry </p>
        </div>
      </div>
    </div>
  </div>
</section>