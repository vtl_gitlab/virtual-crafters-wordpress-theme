<section class="section banner-slider" id="js-banner-slider">
  <?php
  if (have_rows('banner_slider')) :
    while (have_rows('banner_slider')) : the_row();
  ?>
      <div class="banner-slider__slide" style="background-image: url(<?php the_sub_field('banner_slide_bg'); ?>); background-size: cover; background-repeat: no-repeat;">
        <div class="container custom-container py-5">
          <h1 class="mt-0 mb-3"><?php the_sub_field('banner_slider_title'); ?></h1>
          <p><?php the_sub_field('banner_slider_content'); ?></p>
          <a href="<?php the_sub_field('banner_slider_cta'); ?>" class="btn btn-primary">Click me</a>
        </div>
      </div>
  <?php
    endwhile;
  endif;
  ?>
</section>