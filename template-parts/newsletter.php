<section class="newsletter py-5" style="background: #fff1f1;">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="content">
          <h2>SUBSCRIBE TO OUR NEWSLETTER</h2>
          <?php echo do_shortcode(get_field('newsletter_shortcode', 'option')); ?>
        </div>
      </div>
    </div>
  </div>
</section>