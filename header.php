<?php

/**
 * Header file for the Twenty Twenty WordPress default theme.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

?>
<!DOCTYPE html>
<!-- Main Header -->

<head>

	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="google" content="notranslate">
	<meta http-equiv="Content-Language" content="en">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="shortcut icon" href="<?php echo the_field('website_logo', 'option'); ?>" type="image/x-icon">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- #site-header -->
	<header>
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-dark">
				<a class="navbar-brand" href="<?php echo get_bloginfo('wpurl'); ?>">Logo</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>


				<?php
				wp_nav_menu(array(
					'theme_location' => 'top-primary',
					'depth' => 2, // 1 = no dropdowns, 2 = with dropdowns.
					'container' => 'div',
					'container_class' => 'collapse navbar-collapse',
					'container_id' => 'navbarSupportedContent',
					'menu_class' => 'navbar-nav ml-auto',
					'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
					'walker' => new WP_Bootstrap_Navwalker(),
				));
				?>
				<?php get_search_form(); ?>
			</nav>
		</div>
	</header>

	<?php
